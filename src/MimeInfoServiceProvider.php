<?php

namespace Drupal\mimeinfo;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\File\MimeType\MimeTypeGuesser as MimeTypeGuesserCore;
use Drupal\mimeinfo\File\MimeType\MimeTypeGuesser;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpFoundation\File\MimeType\FileinfoMimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\MimeType\FileBinaryMimeTypeGuesser;

/**
 * Class MimeInfoServiceProvider.
 */
class MimeInfoServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $mime_type_guesser_definition = $container->getDefinition('file.mime_type.guesser');
    \assert($mime_type_guesser_definition->getClass() === MimeTypeGuesserCore::class);
    $mime_type_guesser_definition->setClass(MimeTypeGuesser::class);

    foreach ([
      'fileinfo' => FileinfoMimeTypeGuesser::class,
      'filebinary' => FileBinaryMimeTypeGuesser::class,
    ] as $guesser => $class) {
      $service_id = "file.mime_type.guesser.$guesser";

      if (!$container->has($service_id)) {
        $definition = new Definition($class);
        $definition->addTag('mime_type_guesser', ['priority', 1]);

        $container->setDefinition($service_id, $definition);
      }
    }
  }

}
